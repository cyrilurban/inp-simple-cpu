-- file:	cpu.vhd
-- author:	CYRIL URBAN
-- date:	2016-12-22
-- brief:	Simple 8-bit CPU (BrainLove interpreter)

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   				: in std_logic;  -- hodinovy signal
   RESET 				: in std_logic;  -- asynchronni reset procesoru
   EN    				: in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet ROM
   CODE_ADDR 				: out std_logic_vector(11 downto 0); -- adresa do pameti
   CODE_DATA 				: in std_logic_vector(7 downto 0);   -- CODE_DATA <- rom[CODE_ADDR] pokud CODE_EN='1'
   CODE_EN   				: out std_logic;                     -- povoleni cinnosti
   
   -- synchronni pamet RAM
   DATA_ADDR  				: out std_logic_vector(9 downto 0); -- adresa do pameti
   DATA_WDATA 				: out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA 				: in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  				: out std_logic;                    -- cteni (1) / zapis (0)
   DATA_EN    				: out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   				: in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    				: in std_logic;                      -- data platna
   IN_REQ    				: out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA 				: out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY 				: in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   				: out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-------------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

	-- deklarace signalu
	
	signal PC_register    	: std_logic_vector(11 downto 0);
	signal PC_increment    	: std_logic;
	signal PC_decrement    	: std_logic;
	signal PC_allowBus   	: std_logic;
	
	signal PTR_register    	: std_logic_vector(9 downto 0);
	signal PTR_increment    : std_logic;
	signal PTR_decrement    : std_logic;
	signal PTR_allowBus   	: std_logic;	
	
	signal TMP_register    	: std_logic_vector(7 downto 0);
	signal TMP_save    	: std_logic;
	signal TMP_read   	: std_logic;

	signal CNT_register    	: std_logic_vector(11 downto 0) := "000000000000";
	signal CNT_increment    : std_logic;
	signal CNT_decrement    : std_logic;
	signal CNT_makeOne    	: std_logic := '0';
	
	signal mx_selector : std_logic_vector(1 downto 0);

	type instruction_type is (inc_ptr, 
		dec_ptr, 
		inc_val, 
		dec_val, 
		while_start, 
		while_end, 
		print_val, 
		read_val, 
		save_tmp, 
		read_tmp, 
		halt_CPU, 
		skip
	);
	
	signal ireg_dec  		: instruction_type;
	signal ireg_reg  		: std_logic_vector(7 downto 0);
	signal ireg_ld   		: std_logic := '0';
	
	type FSM_state is (
		s_idle, 
		s_fetch0, 
		s_fetch1, 
		s_decode, 
		s_halt_CPU,
		s_skip,
		s_inc_ptr, 
		s_dec_ptr, 
		s_inc_val, 
		s_inc_val2,
		s_dec_val, 
		s_dec_val2,   
		s_print_val,
		s_print_val2, 
		s_read_val, 
		s_read_val2,
		s_save_tmp, 
		s_save_tmp2, 
		s_read_tmp, 
		s_read_tmp2, 	 
		s_while_end,
		s_while_end2, 
		s_while_end3, 
		s_while_end4,
		s_while_end5,
		s_while_start,
		s_while_start2, 
		s_while_start3, 
		s_while_start4,
		s_while_start5	
	);
	
	signal PresentState    	: FSM_state;
	signal NextState    	: FSM_state;
	
begin

	-- Registr PC
	process(CLK, RESET)
	begin
		if (RESET = '1') then -- asynchoronni reset 
			PC_register <= (others=>'0');	

		elsif (CLK'event) and (CLK = '1') then -- nabezna hrana

			if (PC_increment = '1') then -- inkrementuj PC registr
				PC_register <= PC_register + 1;
			end if;

			if (PC_decrement = '1') then -- dekremejtuj PC registr
				PC_register <= PC_register - 1;
			end if;
			
		end if;	
	end process; 
	

	--PC ABUS buffer
	CODE_ADDR <= PC_register when PC_allowBus='1' else
		(others => 'Z');

	-- TMP Registr
	tmpRegiser: process(CLK, RESET)
	begin
		if (RESET = '1') then -- asynchoronni reset
			TMP_register <= (others=>'0');		

		elsif (CLK'event) and (CLK = '1') then -- nabezna hrana

			if (TMP_save = '1') then -- pokyn k ulozeni do TMP registru 
				TMP_register <= DATA_RDATA; -- z RAM pameti
			end if;

		end if;	
	end process; 


	-- Registr instrukci
	process(RESET, CLK)
	begin
		if (RESET='1') then -- asynchoronni reset
		 ireg_reg <= (others=>'0');		

		elsif (CLK'event) and (CLK='1') then

			if (ireg_ld='1') then -- pokyn k ulozeni do instrukcniho registru 
				ireg_reg <= CODE_DATA; -- z ROM pameti
			end if;

		end if;
	end process;

	
	-- Dekoder instrukci
	process(ireg_reg) --dekoduji instrukci z ROM pameti
	begin
		case ireg_reg is
			when X"3E" => ireg_dec <= inc_ptr;  	--">"
			when X"3C" => ireg_dec <= dec_ptr;  	--"<"
			when X"2B" => ireg_dec <= inc_val;  	--"+"
			when X"2D" => ireg_dec <= dec_val;  	--"-"
			when X"5B" => ireg_dec <= while_start;  --"["
			when X"5D" => ireg_dec <= while_end;  	--"]"
			when X"2E" => ireg_dec <= print_val;	--"."
			when X"2C" => ireg_dec <= read_val; 	--","
			when X"24" => ireg_dec <= save_tmp; 	--"$"
			when X"21" => ireg_dec <= read_tmp; 	--"!"
			when X"00" => ireg_dec <= halt_CPU; 	--"halt"
			when others => ireg_dec <= skip; 	-- komentar
		end case;
	end process;
	

	-- Ukazatel do pameti
	process(CLK, RESET)
	begin
		if (RESET = '1') then -- asynchoronni reset
			PTR_register <= (others=>'0');	

		elsif (CLK'event) and (CLK = '1') then --- nabezna hrana

			if (PTR_increment = '1') then -- inkrementuj PTR registr
				PTR_register <= PTR_register + 1;
			end if;

			if (PTR_decrement = '1') then -- dekrementuj PTR registr
				PTR_register <= PTR_register - 1;
			end if;
			
		end if;	
	end process;
	

	--RAM
	DATA_ADDR <= PTR_register when PTR_allowBus='1' else
		(others => 'Z');


	-- multiplexor na volbu hodnoty zapisovane do pameti dat
	process(CLK, mx_selector, DATA_RDATA, IN_DATA)
	begin

		case mx_selector is
			when "00" => DATA_WDATA <= IN_DATA; -- zapis ze vstupu
			when "01" => DATA_WDATA <= DATA_RDATA + 1; -- hodnota aktualni bunky zvysena o 1
			when "10" => DATA_WDATA <= DATA_RDATA - 1; -- hodnota aktualni bunky snizena o 1
			when "11" => DATA_WDATA <= TMP_register; -- hodnota z TMP registru	
			when others =>
		end case;

	end process;


	-- FSM present state
	process(EN, RESET, CLK)
	begin
	  	if (RESET='1') then -- asynchoronni reset
		 	PresentState <= s_idle;

		elsif (CLK'event) and (CLK='1') then

			if(EN = '1') then -- podminena cinnost procesoru
				PresentState <= NextState; 
			end if;

	  end if;
	end process;
	

	-- FSM next state logic
	process(PresentState, ireg_dec)
	
	begin

      DATA_EN <= '0';
	  CODE_EN <= '0';
	  PTR_decrement <= '0';
	  PTR_increment <= '0';
	  PTR_allowBus <= '0';
	  PC_allowBus <= '0';
	  ireg_ld <= '0';
	  PC_decrement <= '0';
	  PC_increment <= '0';
	  OUT_WE <= '0';
	  CNT_makeOne <= '0';
	  CNT_increment <= '0';
	  CNT_decrement <= '0';
	  mx_selector <= "00";
	  TMP_read <= '0';
	  TMP_save <= '0';

		case PresentState is	

			 -- IDLE
			when s_idle =>
				NextState <= s_fetch0;


			 -- NACTENI INSTRUKCE
			when s_fetch0 => 
				PC_allowBus <= '1';
				CODE_EN <= '1';
				NextState <= s_fetch1;

			when s_fetch1 => 
				ireg_ld <= '1';
				NextState <= s_decode;
				

			 -- DEKODOVANI INSTRUKCE
			when s_decode =>
				case ireg_dec is
					when halt_CPU =>
						NextState <= s_halt_CPU;

					when inc_ptr =>
						NextState <= s_inc_ptr;

					when dec_ptr =>
						NextState <= s_dec_ptr;
						
					when inc_val =>
						NextState <= s_inc_val;
						
					when dec_val =>
						NextState <= s_dec_val;
						
					when while_start =>
						NextState <= s_while_start;
						
					when while_end =>
						NextState <= s_while_end;
						
					when print_val =>
						NextState <= s_print_val;

					when read_val =>
						NextState <= s_read_val;

					when read_tmp =>
						NextState <= s_read_tmp;

					when save_tmp =>
						NextState <= s_save_tmp;
						
					when skip =>
						NextState <= s_skip;
				end case;	
			

			-- HALT -zastaveni vykonavani procesoru
			when s_halt_CPU =>
				NextState <= s_halt_CPU;
				

			-- SKIP - ignorace komentaru
			when s_skip =>
				PC_increment <= '1'; -- inkrementace PC registru		
				NextState <= s_fetch0;
				

			-- INCREASE PTR
			when s_inc_ptr =>
				PTR_increment <= '1';
				
				PC_increment <= '1';
				NextState <= s_fetch0;
				

			-- DECREASE PTR
			when s_dec_ptr =>
				PTR_decrement <= '1';
				
				PC_increment <= '1'; 
				NextState <= s_fetch0;
				

			-- INCREASE VAL
			when s_inc_val => --1. faze
				DATA_EN <= '1'; -- povoleni prace s pameti
				PTR_allowBus <= '1'; -- povole ABUSu - sbernice				
				DATA_RDWR <= '1'; -- rezim cteni		
				
				NextState <= s_inc_val2;
			
			when s_inc_val2 => -- 2. faze
				DATA_EN <= '1'; -- povoleni prace s pameti
				PTR_allowBus <= '1'; -- povole ABUSu - sbernice				
				DATA_RDWR <= '0'; -- rezim zapisu

				mx_selector <= "01"; -- vystup multiplexoru provede zapis do pameti

				PC_increment <= '1';
				NextState <= s_fetch0;
				

			-- DECREASE VAL
			when s_dec_val => 
				DATA_EN <= '1';
				PTR_allowBus <= '1';				
				DATA_RDWR <= '1';		
				
				NextState <= s_dec_val2;
			
			when s_dec_val2 =>		
				DATA_EN <= '1';
				PTR_allowBus <= '1';
				DATA_RDWR <= '0';

				mx_selector <= "10"; -- vystup multiplexoru provede zapis do pameti

				PC_increment <= '1';
				NextState <= s_fetch0;
				

			-- PRINT			
			when s_print_val =>
				DATA_EN <= '1';
				PTR_allowBus <= '1';				
				DATA_RDWR <= '1';  -- rezim cteni 		
				
				NextState <= s_print_val2;		
			
			when s_print_val2 =>
			
				if (OUT_BUSY = '0') then -- podminka na zaprazdnenost LCD
					OUT_WE <= '1';
					OUT_DATA <= DATA_RDATA;
		
					PC_increment <= '1';
					NextState <= s_fetch0;

				else NextState <= s_print_val2;
				end if;	
			
			
			-- READ
			when s_read_val =>
				IN_REQ <= '1';			
				NextState <= s_read_val2;
				
			when s_read_val2 =>
				if (IN_VLD = '1') then

					DATA_EN <= '1';
					PTR_allowBus <= '1';			
					DATA_RDWR <= '0'; -- rezim zapisu

					IN_REQ <= '0'; 
					mx_selector <= "00"; -- vystup multiplexoru provede zapis do pameti
										
					PC_increment <= '1';
					NextState <= s_fetch0;	

				else NextState <= s_read_val2;
				end if;				
			

			-- READ TMP (export dat)
			when s_read_tmp => --1. faze
				DATA_EN <= '1'; 
				PTR_allowBus <= '1';							
				DATA_RDWR <= '0'; -- rezim zapisu	

				mx_selector <= "11"; -- vystup multiplexoru provede zapis do pameti

				PC_increment <= '1';
				NextState <= s_fetch0;


			-- SAVE TMP (import dat)
			when s_save_tmp =>
				TMP_save <= '1'; -- prikaz k ulozeni do TMP registru

				NextState <= s_save_tmp2;

			when s_save_tmp2 =>
				DATA_EN <= '1';	
				PTR_allowBus <= '1';
				DATA_RDWR <= '1'; -- rezim cteni
		
				PC_increment <= '1';
				NextState <= s_fetch0;
				

			--WHILE
			when s_while_start =>

				DATA_EN <= '1'; --povol praci s pameti
				PTR_allowBus <= '1'; --povol sbernici				
				DATA_RDWR <= '1'; --bude se cist	

				PC_increment <= '1'; --zvys hodnotu PC registru
				NextState <= s_while_start2;			
				
			when s_while_start2 =>
				if (DATA_RDATA = "00000000") then
					NextState <= s_while_start3;				
				else
					NextState <= s_fetch0;
				end if;				
				
			when s_while_start3 =>	
				PC_increment <= '1'; --zvys hodnotu PC registru

				PC_allowBus <= '1';
				CODE_EN <= '1';
				NextState <= s_while_start4;
				
			when s_while_start4 =>	  
				ireg_ld <= '1';
				NextState <= s_while_start5;

			when s_while_start5 =>  

				if (ireg_reg = "000001011101") then -- ]
					NextState <= s_fetch0; --vyskoc z ignorovani while			
				else
					NextState <= s_while_start3;
				end if;
				
				
			--END WHILE			
			when s_while_end =>				
				DATA_EN <= '1'; --povol praci s pameti
				PTR_allowBus <= '1'; --povol sbernici				
				DATA_RDWR <= '1'; --bude se cist	
				
				NextState <= s_while_end2;			
				
			when s_while_end2 =>	  
				if (DATA_RDATA /= "00000000") then					
					NextState <= s_while_end3;	
				else
					NextState <= s_fetch0; --vyskoc z ignorovani while	
					PC_increment <= '1'; --zvys hodnotu PC registru				
				end if;				
				
			when s_while_end3 =>
				PC_decrement <= '1';

				PC_allowBus <= '1';
				CODE_EN <= '1';
				NextState <= s_while_end4;
				
			when s_while_end4 =>	  
				ireg_ld <= '1';
				NextState <= s_while_end5;
			
			when s_while_end5 =>

				if (ireg_reg = "000001011011") then -- [
					NextState <= s_fetch0; --vyskoc z ignorovani while	
					PC_increment <= '1'; --zvys hodnotu PC registru	
				else			
					NextState <= s_while_end3;
				end if;
						
			when others => 
				NextState <= s_fetch0;
		end case;
	end process;

end behavioral;

